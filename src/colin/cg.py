from numpy import *

def solve_cg(A,b,x0,rtol,maxits):
    """
    Function to use the conjugate gradient algorithm to
    solve the equation Ax = b for symmetric positive definite A.
    Inputs:
    A -- An nxn matrix stored as a rank-2 numpy array
    b -- A length n vector stored as a rank-1 numpy array
    x0 -- The initial guess, length n vector stored as a rank-1 numpy array
    rtol -- a tolerance, algorithm should stop if l2-norm of
    the residual r=Ax-b drops below this value.
    maxits -- Stop if the tolerance has not been reached after this number
    of iterations

    Outputs:
    x -- the approximate solution
    rvals -- a numpy array containing the l2 norms of the residuals
    r=Ax-b at each iteration
    """


    rvals = []
    x = x0.copy()
    r_old = b.copy()-x0.copy()
    error = linalg.norm(r_old)



    for iteration in range(maxits):


        rvals.append(error)

        if error < rtol:

            break

        if iteration == 0:
            p = r_old

        else:
            p = r_old + ((dot(transpose(r_old),r_old))
                      /(dot(transpose(r_older),r_older)))*p

        alpha = dot( transpose(r_old),r_old) / dot(transpose(p), dot(A,p) )
        x += alpha*p
        r = r_old - alpha*dot(A,p)

        error = sqrt(dot(transpose(r), r))

        r_older = r_old.copy()
        r_old = r.copy()



    return x, rvals
